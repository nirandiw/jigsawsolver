package com.jigsaw.solver;

import com.jigsaw.Regression.LinearRegression;
import com.jigsaw.analysis.ErrorAnalysis;
import com.jigsaw.generalUtils.ConstantValues;
import com.jigsaw.generalUtils.FileUtils;
import com.jigsaw.imageUtils.*;
import com.jigsaw.lowResolutionImage.CalculateProb;
import com.jigsaw.lowResolutionImage.ClusterCenter;
import com.jigsaw.lowResolutionImage.Histogram;
import com.jigsaw.modelInference.LoopyBeliefPropagationV1;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class JigsawSolver {

	static int patchWidth = ConstantValues.PATCH_WIDTH, patchHeight = ConstantValues.PATCH_HEIGHT;
	public static final int totalAnchors = 2;
	static int imgHeight = 500, imgWidth = 1000;
	static int method=3;

	public static void main(String args[]) {

		imageRescaling(patchWidth, patchHeight, imgHeight, imgWidth);

		ImageUtils imageUtils = new ImageUtils();
		File imageFile = new File("Run/scale_image.png");

		imageUtils.createImageRGB(imageUtils.setImage(imageFile, patchHeight,
				patchWidth));
		ProbabilityTableMaker probabilityTableMaker = new ProbabilityTableMaker(imageUtils, new ImagePatchDistance(patchHeight,
                patchWidth));

		// Returns the potential function value matrix
		probabilityTableMaker.getDistanceMatrix(imageFile);

		// createPatches(imageUtils, imageFile, patchHeight, patchWidth);

		HashMap<Integer, Integer> anchorNodes = AnchoredImage.getAnchors(
				imageUtils, totalAnchors);
		System.out.println(anchorNodes.size());



		LoopyBeliefPropagationV1 loopyBelief = new LoopyBeliefPropagationV1(
				imageUtils, anchorNodes, probabilityTableMaker);
		
		// Method = 1 -> Only Anchor Patches
		// Method = 2 -> With Original Low Resolution Image
		// Method = 3 -> With Learned Low Resolution Image
		
		if (method == 1) {
			loopyBelief.setLikelihood(null);
		}
		if (method == 2) {
			double[][] likelihood_original = getLikelihoodOriginal(imageUtils);
			loopyBelief.setLikelihood(likelihood_original);
			System.out.println("Likelihood Matrix : ");
			for (int i=0;i<likelihood_original.length;i++) {
				for (int j=0;j<likelihood_original[0].length;j++) {
					System.out.print(likelihood_original[i][j] + " ");
				}
				System.out.println("\n");
			 }
		}
		else if (method == 3) {
			double[][] likelihood_learned = getLikelihoodLearned(imageUtils);
			loopyBelief.setLikelihood(likelihood_learned);
			System.out.println("Likelihood Matrix : ");
			for (int i=0;i<likelihood_learned.length;i++) {
				for (int j=0;j<likelihood_learned[0].length;j++) {
					System.out.print(likelihood_learned[i][j] + " ");
				}
				System.out.println("\n");
			 }
		}
		
		loopyBelief.run();

		File resultImage = new File("result.png");
		ImageUtils imageUtils1 = new ImageUtils();
		ImageReconstruct.reconstructImage(loopyBelief.assignLabel(),imageUtils, anchorNodes,resultImage);
		
		imageUtils1.createImageRGB(imageUtils1.setImage(resultImage, patchHeight,
				patchWidth));
		showError(imageUtils, imageUtils1);

	}

	private static double[][] getLikelihoodOriginal(ImageUtils imageUtils) {

		File lowres_image = new File("Run/low_res_original.png");
		ImageUtils imgutils = new ImageUtils();
		imgutils.createImageRGB(imgutils.setImage(lowres_image,
				imageUtils.getMaxPatchHeight(), imageUtils.getMaxPatchWidth()));
		
		int[][] lowresArray = CalculateProb.lowResolutionImageArray(lowres_image);
		double[][] likelihood = CalculateProb.calculate_Likelihood1(lowresArray,imageUtils);

		return likelihood;
	}

	private static double[][] getLikelihoodLearned(ImageUtils imageUtils) {

		ArrayList<ClusterCenter> listOfClusterCenters = new
				ArrayList<ClusterCenter>();

		ClusterCenter center_patch =new ClusterCenter(patchHeight,patchWidth); 
		try { 
				Scanner in = new Scanner(new File("clustercenters.txt")); 
				for (int i = 0; i < 5; i++)
				{
					in.nextInt(); 
					for (int k = 0; k < patchHeight; k++) { 
						for (int l = 0;l < patchWidth; l++) { 
							center_patch = new ClusterCenter(patchHeight,patchWidth);  
							center_patch.patchRGB[k][l]=in.nextInt(); 
						} 
					}
					listOfClusterCenters.add(center_patch); 
				} 
				in.close(); 
			} catch (IOException e) { }

		Histogram problemHistogram = new Histogram(listOfClusterCenters);
		int[] histogram = problemHistogram.generateHistogramsForTheProblem(imageUtils);

		for (int i = 0; i < histogram.length; i++)
			System.out.println(histogram[i]);

		String[] files = { "lowresolutionimageREDtheta.txt",
				"lowresolutionimageGREENtheta.txt", "lowresolutionimageBLUEtheta.txt"}; 
		int sizeLowResolutionImage = (imgHeight * imgWidth) / (patchHeight* patchWidth); 
		int components = 3; 
		int clusterCenters = 5;

		double[][] lowResolutionImage = new double[components][sizeLowResolutionImage]; 
		int current = 0; 
		for	(String element : files) {
			double[][] theta = new double[sizeLowResolutionImage][clusterCenters+ 1];
			Scanner in; try { in = new Scanner(new File(element));

			for (int i = 0; i < sizeLowResolutionImage; i++) 
				for (int j = 0; j < clusterCenters + 1; j++) { 
					theta[i][j] = in.nextDouble(); 
				}
				in.close(); 
			} catch (FileNotFoundException e) { }

			lowResolutionImage[current] = LinearRegression.predict_component(
					histogram, theta, sizeLowResolutionImage); 
			current++; 
		} 
		
		for (int j =0; j < lowResolutionImage[0].length; j++) { 
			System.out.println((int)lowResolutionImage[0][j] + " " + (int) lowResolutionImage[1][j] + " "
							+ (int) lowResolutionImage[2][j]); 
		}

		int lowHeight = imgHeight / patchHeight; 
		int lowWidth = imgWidth / patchWidth; 
		BufferedImage lowResolutionBuffer = new BufferedImage(lowWidth, lowHeight, BufferedImage.TYPE_INT_RGB);

		for (int h = 0; h < lowHeight; h++) { 
			for (int w = 0; w < lowWidth;w++) { 
				int rgb = IntegerToRGBConverter.getIntFromColor( (int)
						lowResolutionImage[0][h * lowWidth + w], (int)
						lowResolutionImage[1][h * lowWidth + w], (int)
						lowResolutionImage[2][h * lowWidth + w]);

				lowResolutionBuffer.setRGB(w, h, rgb & 0x00FFFFFF); 
			} 
		} 
		try { 
			ImageIO.write(lowResolutionBuffer, "png", new File("Run/low_res_learned.png")); 
		}
		catch (Exception e) { e.printStackTrace(); }

			File lowres_image = new File("Run/low_res_learned.png");
			ImageUtils imgutils = new ImageUtils();
			imgutils.createImageRGB(imgutils.setImage(lowres_image,imageUtils.getMaxPatchHeight(), imageUtils.getMaxPatchWidth()));
			int[][] lowresArray = CalculateProb.lowResolutionImageArray(lowres_image);
			double[][] likelihood = CalculateProb.calculate_Likelihood2(lowresArray,imageUtils);

			return likelihood;
	}

	private static void showError(ImageUtils imageUtils, ImageUtils imageUtils1) {
		double acc1 = ErrorAnalysis.direct_compare(imageUtils, imageUtils1);
		double acc2 = ErrorAnalysis.neighbors_comparison(imageUtils, imageUtils1);

		System.out.println("Direct Compare Accuracy : " + acc1);
		System.out.println("Neighbor Compare Accuracy : " + acc2);
	}

	private static void imageRescaling(int patchWidth, int patchHeight,
			int imgHeight, int imgWidth) {

		BufferedImage originalImage;
		try {
			originalImage = ImageIO.read(new File("Run/6.png"));
			int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB
					: originalImage.getType();

			BufferedImage resizeImagePng = RescaleImage.resizeImage(
					originalImage, type, imgWidth, imgHeight);
			ImageIO.write(resizeImagePng, "png", new File(
					"Run/scale_image.png"));

			BufferedImage resizeImgPng = RescaleImage.resizeImage(
					originalImage, type, imgWidth / patchWidth, imgHeight
							/ patchHeight);
			ImageIO.write(resizeImgPng, "png", new File(
					"Run/low_res_original.png"));

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	private static void createPatches(ImageUtils imageUtils, File imageFile,
			int patchHeight, int patchWidth) {
		File patchFolder = new File("image1");
		FileUtils.createDirectory(patchFolder);

		try {
			ImageUtils.createImagePatches(patchHeight, patchWidth,
					imageUtils.getMaxPatchHeight() * patchHeight,
					imageUtils.getMaxPatchWidth() * patchWidth,
					patchFolder.getAbsolutePath(), imageFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
