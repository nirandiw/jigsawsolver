package com.jigsaw.solver;

import java.io.File;

import com.jigsaw.imageUtils.ImagePatchDistance;
import com.jigsaw.imageUtils.ImageUtils;
import com.jigsaw.modelInference.probabilisticGraph.Direction;

public class ProbabilityTableMaker {

    private static final double BIAS = 10;
    private ImageUtils imageUtil;
    private ImagePatchDistance patchDistance;
    private double[][][] matrix;

    public ProbabilityTableMaker(final ImageUtils imageUtil, final ImagePatchDistance patchDistance) {
        this.imageUtil = imageUtil;
        this.patchDistance = patchDistance;
    }

    public void getDistanceMatrix(final File imageFile) {

        matrix = new double[imageUtil.getTotalPatchNumber()][imageUtil.getTotalPatchNumber()][4];

        getDistanceMatrix(matrix);

        getCompatibilityMatrix(matrix);
        // test matrix
        // showDistanceMarix(matrix);
    }

    // return edge potential between patch1 and patch2. The direction is w.r.t
    // patch1.
    public double getPotential(final int patch1, final int patch2, final Direction direction) {
        if ("factor".equals(direction.getName())) {
            System.out.println("ERROR : no potential for factor nodes");
        }
        return matrix[patch1][patch2][direction.getValue()];
    }

    @SuppressWarnings("unused")
    private void showDistanceMarix(final double[][][] matrix) {
        System.out.println("showing matrix");
        for (int i = 0; i < imageUtil.getTotalPatchNumber(); i++) {
            System.out.println();
            for (int j = 0; j < imageUtil.getTotalPatchNumber(); j++) {
                System.out.print(matrix[i][j][0] + "\t");
            }
        }

    }

    private void getCompatibilityMatrix(double[][][] matrix) {

        for (int i = 0; i < imageUtil.getTotalPatchNumber(); i++) {
            for (int k = 0; k < 4; k++) {
                double sigma = getSigma(matrix, i, k);
                for (int j = 0; j < imageUtil.getTotalPatchNumber(); j++) {

                    if (i == j) {
                        continue;
                    }

                    matrix[i][j][k] = (double) (-matrix[i][j][k] / (2 * Math.pow(sigma, 1)));
                    matrix[i][j][k] = Math.exp(matrix[i][j][k]);
                }
            }
        }

    }

    private double getSigma(final double[][][] matrix, int patch, int colorComponent) {

        double firstMin = 0, secondMin = 0;
        boolean isNotInitialised = true;

        for (int j = 0; j < imageUtil.getTotalPatchNumber(); j++) {
            if (j == patch)
                continue;

            if (isNotInitialised) {
                /*
                 * discuss with the others what happens when the distance become
                 * equal and those become the min and second min. sigma will be
                 * zero! minDistance = table[i][j][k];
                 */

                firstMin = Math.min(matrix[patch][j][colorComponent], matrix[patch][j + 1][colorComponent]);
                secondMin = Math.max(matrix[patch][j][colorComponent], matrix[patch][j + 1][colorComponent]);
                isNotInitialised = false;
            }

            else if (firstMin > matrix[patch][j][colorComponent]) {
                secondMin = firstMin;
                firstMin = matrix[patch][j][colorComponent];
            }

            else if (secondMin > matrix[patch][j][colorComponent]) {
                secondMin = matrix[patch][j][colorComponent];
            }
        }

        if ((secondMin - firstMin) > 0) {
            return (secondMin - firstMin + BIAS);
        }

        else {
            // System.out.println(" Warning : ArithmeticException :  sigma = 0 !"
            // + firstMin + ": Patch :  " + patch);
            return (secondMin - firstMin + BIAS);
        }
    }

    private void getDistanceMatrix(double[][][] matrix) {


        for (int i = 0; i < imageUtil.getMaxPatchHeight(); i++) {
            for (int j = 0; j < imageUtil.getMaxPatchWidth(); j++) {
                int[][] patch1 = imageUtil.getImagePatch(i, j);

                for (int k = i; k < imageUtil.getMaxPatchHeight(); k++) {
                    for (int l = 0; l < imageUtil.getMaxPatchWidth(); l++) {
                        if (k == i && l <= j)
                            continue;
                        else {
                            int[][] patch2 = imageUtil.getImagePatch(k, l);
                            //showPatch(patch1,patch2);
                            matrix[i * imageUtil.getMaxPatchWidth() + j][k * imageUtil.getMaxPatchWidth() + l] = patchDistance
                                    .getPatchDist(patch1, patch2);

                            /* fill the other half of the distance matrix */
                            matrix[k * imageUtil.getMaxPatchWidth() + l][i * imageUtil.getMaxPatchWidth() + j] = patchDistance
                                    .getPatchDist(patch2, patch1);

                        }
                    }
                }
            }
        }
    }

    private void showPatch(int[][] patch1, int[][] patch2) {
        for(int h=0;h<200;h++){
            System.out.println();
            for(int w=0;w<200;w++){
                System.out.print("("+patch1[h][w]+","+patch2[h][w]+")");
            }
        }
        System.out.println("wait");
    }
}
