package com.jigsaw.solver;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;

import com.jigsaw.imageUtils.ImageUtils;

public class ImageReconstruct {

    public static void reconstructImage(int[][] assignLabel, ImageUtils imageUtils, HashMap<Integer,Integer> anchorNodes, File file) {
        BufferedImage image = new BufferedImage(imageUtils.getImageWidth(), imageUtils.getImageHeight(),
                BufferedImage.TYPE_INT_RGB);

        for (int h = 0; h < imageUtils.getMaxPatchHeight(); h++) {
            for (int w = 0; w < imageUtils.getMaxPatchWidth(); w++) {
                int originalHeight = assignLabel[h][w] / imageUtils.getMaxPatchWidth();
                int originalWidth = assignLabel[h][w] % imageUtils.getMaxPatchWidth();
                boolean isAnchored =false;
                if(anchorNodes.containsKey(h*imageUtils.getMaxPatchWidth()+w)){
                    isAnchored = true;
                }
                
                fillBuffer(image, imageUtils.getPatchStartPosition(h, w),
                        imageUtils.getImagePatch(originalHeight, originalWidth), imageUtils.getPatchHeight(),
                        imageUtils.getPatchWidth(), isAnchored);
            }
        }
        
        try {
            ImageIO.write( image, "png", file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void fillBuffer(BufferedImage bufferedImage, int[] patchStartPosition, int[][] patch,
            int patchHeight, int patchWidth, boolean isAnchored) {

        for (int h = patchStartPosition[0]; h < patchStartPosition[0] + patchHeight; h++) {
            for (int w = patchStartPosition[1]; w < patchStartPosition[1] + patchWidth; w++) {
                int rgb = patch[h-patchStartPosition[0]][w-patchStartPosition[1]] & 0x00FFFFFF;
                if(isAnchored){
                    rgb = rgb | 0x00FF0000;
                }
                bufferedImage.setRGB(w,h,rgb);
            }
        }
    }
}
