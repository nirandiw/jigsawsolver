package com.jigsaw.generalUtils;

/**
 * Created by nirandikawanigasekara on 10/11/14.
 */
public class ConstantValues {

    public static String LOW_RESOLUTION_IMAGE_OUTPUT_PATH="LowImageFolder/";
    public static String SAME_SIZE_IMAGE_OUTPUT_PATH="ImageFolder/";
    public static String ORIGINAL_IMAGE_PATH="rawimages";
	public static int PATCH_HEIGHT=50;
	public static int PATCH_WIDTH=50;
	public static int LOWRES_HEIGHT=500/PATCH_HEIGHT;
	public static int LOWRES_WIDTH=1000/PATCH_WIDTH;
}
