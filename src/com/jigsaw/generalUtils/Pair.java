package com.jigsaw.generalUtils;

public class Pair {
    public int height;
    public int width;

    public Pair(int h, int w) {
        this.height = h;
        this.width = w;
    }
}
