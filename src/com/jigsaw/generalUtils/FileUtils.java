package com.jigsaw.generalUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {

    /* returns all the image files iteratively */
    public static List<String> setPath(final File sourceDirectory) throws IOException {

        if (!sourceDirectory.exists()) {
            System.out.println("ERROR: source directory NOT FOUND!");
        } else if (!sourceDirectory.isDirectory()) {
            System.out.println("ERROR: Not a source directory");
        } else if (sourceDirectory.list().length == 0) {
            System.out.println("ERROR: No image file inside");
        }

        List<String> myImageList = new ArrayList<String>();
        getAllImageFile(sourceDirectory, myImageList);

        return myImageList;
    }

    private static void getAllImageFile(final File file, List<String> imageList) throws IOException {

        if (file.isDirectory()) {
            if (file.list().length == 0) {
                return;
            } else {
                // list all the directory contents
                String files[] = file.list();

                for (String temp : files) {
                    // construct the file structure
                    File fileLocal = new File(file, temp);
                    // recursive search
                    getAllImageFile(fileLocal, imageList);
                }
            }
        } else {
            // if file, check if image file and then return
            if (checkSuffix(file.getAbsolutePath())) {
                imageList.add(file.getAbsolutePath());
            }
        }
    }

    private static boolean checkSuffix(String string) {
        return (string.matches("(.*)jpg") | string.matches("(.*)png") | string.matches("(.*)jpeg"));
    }

    public static void createDirectory(File file) {
        if (!file.exists()) {
            file.mkdir();
        }
    }
}
