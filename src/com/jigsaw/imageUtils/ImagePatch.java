package com.jigsaw.imageUtils;

import java.io.File;
import java.util.List;

public class ImagePatch {

    List<String> imageList;
    int patchHeight, patchWidth;
    ImageUtils imageUtils;

    int currentImageNumber;

    // Local Variables
    File currentImage;
    int nextPatchHeight, nextPatchWidth;

    public ImagePatch(ImageUtils myImageUtils, final List<String> myImageList, int myPatchHeight, int myPatchWidth) {
        this.imageList = myImageList;
        patchHeight = myPatchHeight;
        patchWidth = myPatchWidth;
        imageUtils = myImageUtils;

        initialize();
    }

    private void initialize() {
        if (imageList.size() == 0) {
            System.out.println("ERROR: no image in the list");
        } else {
            currentImageNumber = 0;
            updateToNextImage();
        }
    }

    public void reset(){
        initialize();
    }

    public int getImageCounter(){
        return currentImageNumber;
    }

    public int[][] getNextImagePatch() {
        if (imageList.size() > currentImageNumber) {
            int[][] currentPatch = imageUtils.getImagePatch(nextPatchHeight, nextPatchWidth);
            updateNextPatch();
            return currentPatch;
        } else {
            return null;
        }

    }

    private void updateNextPatch() {
        nextPatchWidth = nextPatchWidth + 1;

        if (nextPatchWidth == imageUtils.getMaxPatchWidth()) {
            nextPatchWidth = 0;
            nextPatchHeight = nextPatchHeight + 1;
        }
        if (nextPatchHeight == imageUtils.getMaxPatchHeight()) {

            // if this is not the last image
            currentImageNumber++;
            updateToNextImage();
        }

    }

    private void updateToNextImage() {
        if (imageList.size() >currentImageNumber) {
            currentImage = new File(imageList.get(currentImageNumber));
            System.out.println("currentImageNumber  :  "+imageList.get(currentImageNumber));
            // take next image
            nextPatchWidth = 0;
            nextPatchHeight = 0;
            System.out.print("update to next image" + currentImageNumber);
            System.out.println("  "+imageList.size());
            // update imageUtils
            imageUtils.createImageRGB(imageUtils.setImage(currentImage, patchHeight, patchWidth));
        } else {
            return;
        }
    }

}
