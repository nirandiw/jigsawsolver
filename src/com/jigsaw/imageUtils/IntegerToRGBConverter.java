package com.jigsaw.imageUtils;

import java.awt.Color;
import java.util.Random;

public class IntegerToRGBConverter {

    private static final int RED = 0;
    private static final int GREEN = 1;
    private static final int BLUE = 2;

    public static int[] getComponents(final int myColor) {
        int[] components = new int[3];
        Color color = new Color(myColor);
        components[RED] = color.getRed();
        components[BLUE] = color.getBlue();
        components[GREEN] = color.getGreen();
        return components;
    }

    public static int getIntFromColor(int red, int green, int blue) {
        red = (red << 16) & 0x00FF0000; // Shift red 16-bits and mask out other
                                        // stuff
        green = (green << 8) & 0x0000FF00; // Shift Green 8-bits and mask out
                                           // other stuff
        blue = blue & 0x000000FF; // Mask out anything not blue.

        return 0x0F000000 | red | green | blue; // 0xFF000000 for 100% Alpha.
                                                // Bitwise OR everything
                                                // together.
    }

    public static int getRandomColor() {

        Random rand = new Random(256);
        int red = (rand.nextInt() << 16) & 0x00FF0000; // Shift red 16-bits and
                                                       // mask out other stuff
        int green = (rand.nextInt() << 8) & 0x0000FF00; // Shift Green 8-bits
                                                        // and mask out other
                                                        // stuff
        int blue = rand.nextInt() & 0x000000FF; // Mask out anything not blue.

        // TODO change alpha value
        return 0x0F000000 | red | green | blue; // 0xFF000000 for 100% Alpha.
                                                // Bitwise OR everything
                                                // together.
    }
}
