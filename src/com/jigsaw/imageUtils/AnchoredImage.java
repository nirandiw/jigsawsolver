package com.jigsaw.imageUtils;

import java.util.HashMap;
import java.util.Random;

public class AnchoredImage {

    public static HashMap<Integer, Integer> getAnchors(ImageUtils imageUtils, int totalAnchors) {
        HashMap<Integer, Integer> anchorMap = new HashMap<Integer, Integer>();
        Random random = new Random(System.currentTimeMillis());

//        for (int i = 0; i < totalAnchors; i++) {
//            int anchorPosition = random.nextInt(imageUtils.getTotalPatchNumber());
//            System.out.println(imageUtils.getTotalPatchNumber() + "anchorPosition" + anchorPosition);
//            // we are actually using identical mapping till now.
//            anchorMap.put(anchorPosition, anchorPosition);
//        }
        //hard-code
        anchorMap.put(21, 21);
          anchorMap.put(125, 125);
        anchorMap.put(187, 187);
        anchorMap.put(66, 66);
        anchorMap.put(159, 159);
        anchorMap.put(148, 148);
        anchorMap.put(134, 134);

        System.out.println("size" + anchorMap.size());
        return anchorMap;
    }
}
