package com.jigsaw.imageUtils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

/*
 * Basic image operations
 */
public class ImageUtils {

    private int imageHeight;
    private int imageWidth;

    private int patchWidth;
    private int patchHeight;

    private int maxPatchHeight;
    private int maxPatchWidth;

    private int[][] imageBlock;

    public BufferedImage setImage(final File imageFile, final int patchHeight, final int patchWidth) {

        BufferedImage imageBuffer;
        try {
            imageBuffer = ImageIO.read(imageFile);
            this.imageHeight = imageBuffer.getHeight();
            this.imageWidth = imageBuffer.getWidth();

            // System.out.println(imageHeight);
            // System.out.println(imageWidth);

            this.patchHeight = patchHeight;
            this.patchWidth = patchWidth;

            setMaxPatchHeight();
            setMaxPatchWidth();

            return imageBuffer;
        } catch (IOException e) {
            System.out.println("Error while reading the image: " + e);
        }
        return null;
    }

    private void setMaxPatchWidth() {
        maxPatchWidth = imageWidth / patchWidth;
    }

    private void setMaxPatchHeight() {
        maxPatchHeight = imageHeight / patchHeight;
    }

    /**
     * @return image height (pixel-height)
     */
    public int getImageHeight() {
        return imageHeight;
    }

    /**
     * @return image width (pixel-width)
     */
    public int getImageWidth() {
        System.out.println("here");
        return imageWidth;
    }

    public int getMaxPatchWidth() {
        return maxPatchWidth;
    }

    public int getMaxPatchHeight() {
        return maxPatchHeight;
    }

    public int getTotalPatchNumber() {
        return maxPatchHeight * maxPatchWidth;
    }

    public int getPatchWidth() {
        return patchWidth;
    }

    public int getPatchHeight() {
        return patchHeight;
    }

    /* returns the R[][] G[][] B[][] values given an image(piece) file */
    public void createImageRGB(final BufferedImage imageBuffer) {

        int[] rgbArray = imageBuffer.getRGB(0, 0, imageWidth, imageHeight, null, 0, imageWidth);

        imageBlock = new int[imageHeight][imageWidth];

        for (int h = 0; h < imageHeight; h++) {
            for (int w = 0; w < imageWidth; w++) {
                imageBlock[h][w] = rgbArray[h * imageWidth + w];
            }
        }
    }

    /**
     * Create image patches given image file and location to store.
     * Computationally heavy: we are not going to use these patches (by storing
     * into the disk).
     * 
     * This method is completely separate and independent from the rest of the
     * class; should be moved to separate class.
     * 
     * @param patchHeight
     * @param patchWidth
     * @param maxHeight
     * @param maxWidth
     * @param location
     *            Directory location to store the patches.
     * @param imageFile
     * @throws IOException
     */
    public static void createImagePatches(final int patchHeight, final int patchWidth, final int maxHeight,
            final int maxWidth, final String location, final File imageFile) throws IOException {

        BufferedImage imageBuffer = ImageIO.read(imageFile);
        int size[] = new int[2];

        size[0] = imageBuffer.getHeight();
        size[1] = imageBuffer.getWidth();

        if (maxHeight > size[0] || maxWidth > size[1]) {
            System.out.println("ERROR: size: maxHeight > size[Height] || maxWidth > size[Width]");
        }

        BufferedImage imagePatch = null;
        int patchRow = 1, patchCol = 1;
        for (int h = 0; h < maxHeight; h++) {
            for (int w = 0; w < maxWidth; w++) {
                imagePatch = null;
                imagePatch = imageBuffer.getSubimage(w, h, patchWidth, patchHeight);

                /* save image patches as .png */
                ImageIO.write(imagePatch, "png", new File(location + "/patch_" + patchRow + "_" + patchCol + ".png"));

                w += patchWidth - 1;
                patchCol++;
            }
            h += patchHeight;
            patchRow++;
            patchCol = 0;
        }
    }

    public int[][] getImagePatch(final int patchHeightNum, final int patchWidthNum) {

        int startPixel[] = getPatchStartPosition(patchHeightNum, patchWidthNum);
        //System.out.println("start(h,w)"+ startPixel[0] +" "+ startPixel[1]);
        return getImageBlock(startPixel, patchHeight, patchWidth);
    }

    private int[][] getImageBlock(int[] startPixel, int patchHeight, int patchWidth) {

        int[][] patchBlock = new int[patchHeight][patchWidth];

        for (int h = 0; h < patchHeight; h++) {
            for (int w = 0; w < patchWidth; w++) {
                patchBlock[h][w] = imageBlock[startPixel[0] + h][startPixel[1] + w];
            }
        }
        return patchBlock;
    }

    public int[] getPatchStartPosition(final int patchHeightNum, final int patchWidthNum) {

        int startPixel[] = new int[2];

        startPixel[0] = patchHeightNum * patchHeight;
        startPixel[1] = patchWidthNum * patchWidth;

        return startPixel;
    }

    public int getPixel(int h, int w) {
        return imageBlock[h][w];
    }

    public int[][] getImageBlock() {
        return imageBlock;
    }

    public void testReplace(int[][] image, int height, int width) {
        imageBlock = image;
        imageHeight = height;
        imageWidth = width;
    }

}