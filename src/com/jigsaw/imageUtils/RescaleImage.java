package com.jigsaw.imageUtils;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

public class RescaleImage {

	public static BufferedImage resizeImage(BufferedImage originalImage, int type, int imgWidth, int imgHeight){
		BufferedImage resizedImage = new BufferedImage(imgWidth, imgHeight, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, imgWidth, imgHeight, null);
		g.dispose();

		return resizedImage;
	}
}
