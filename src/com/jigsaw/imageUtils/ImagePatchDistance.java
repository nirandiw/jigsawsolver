package com.jigsaw.imageUtils;

public class ImagePatchDistance {

	private int patchHeight, patchWidth;

	public ImagePatchDistance(final int patchHeight, final int patchWidth) {

		/* in our case: patchHeight == patchWidth */
		this.patchHeight = patchHeight;
		this.patchWidth = patchWidth;
	}

	/*
	 * Can be extend later (if needed); for now don't consider about the
	 * rotation of patches !! Only LR, RL, TB, BT distances are measured
	 */

	public double[] getPatchDist(final int[][] patch1, final int[][] patch2) {

		double[] dist = new double[4]; // LR, RL, TB, BT

		int[] oneLeft = new int[patchHeight];
		int[] oneRight = new int[patchHeight];

		int[] twoLeft = new int[patchHeight];
		int[] twoRight = new int[patchHeight];

		// extract LR vectors
		for (int i = 0; i < patchHeight; i++) {
			oneLeft[i] = patch1[i][patchWidth - 1];
			oneRight[i] = patch1[i][0];

			twoLeft[i] = patch2[i][patchWidth - 1];
			twoRight[i] = patch2[i][0];
		}

		// extract TB vectors
		int[] oneTop = new int[patchWidth];
		int[] oneBot = new int[patchWidth];

		int[] twoTop = new int[patchWidth];
		int[] twoBot = new int[patchWidth];

		for (int i = 0; i < patchWidth; i++) {
			oneTop[i] = patch1[patchHeight - 1][i];
			oneBot[i] = patch1[0][i];

			twoTop[i] = patch2[patchHeight - 1][i];
			twoBot[i] = patch2[0][i];
		}

		dist[0] = getDist(oneLeft, twoRight);
		dist[1] = getDist(oneRight, twoLeft);

		dist[2] = getDist(oneTop, twoBot);
		dist[3] = getDist(oneBot, twoTop);

		return dist;
	}

	/* calculate distance between two vectors */
	private double getDist(int[] one, int[] two) {

		double dist = 0;
		int lab1[] = new int[3];
		int lab2[] = new int[3];

		for (int i = 0; i < one.length; i++) { // one.length == two.length

			ConvertRGBToLAB.rgb2lab(one[i], lab1);
			ConvertRGBToLAB.rgb2lab(two[i], lab2);
			for (int j = 0; j < 3; j++) {
				dist = dist + Math.pow(lab1[j] - lab2[j], 2);
			}
			dist = dist + Math.pow(rgbDiff(one[i], two[i]), 2);

		}
		return dist / one.length;
	}

	private double rgbDiff(int rgb1, int rgb2) {
		int[] comp1 = IntegerToRGBConverter.getComponents(rgb1);
		int[] comp2 = IntegerToRGBConverter.getComponents(rgb2);
		double dist = 0;
		for (int i = 0; i < 3; i++) {
			dist += comp1[i] - comp2[i];
		}
		return dist;
	}
}
