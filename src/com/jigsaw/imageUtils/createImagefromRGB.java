package com.jigsaw.imageUtils;

import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

public class createImagefromRGB {
	
	public static void create_image(int[][] pixels) {
		
		System.out.println("Write Height " + pixels.length);
		System.out.println("Write Width " + pixels[0].length);
		BufferedImage img = new BufferedImage(pixels.length, pixels[0].length, BufferedImage.TYPE_INT_RGB);
		for (int i=0;i<pixels.length;i++) {
			for (int j=0;j<pixels[0].length;j++) {
				img.setRGB(i, j, pixels[i][j] & 0x00FFFFFF);
			}
		}
		File f = new File("C:/Users/Shah/Documents/workspace/JigsawSolver/ImageFolder/output_image.png");
		try {
			ImageIO.write(img, "PNG", f);
			System.out.println("File Written");
		} catch (Exception e) 
		{ System.out.print("Error writing File"); }
	}
	
}
