package com.jigsaw.modelInference.probabilisticGraph;

import java.util.ArrayList;

import com.jigsaw.modelInference.LoopyBeliefPropagationV1;
import com.jigsaw.modelInference.Thresholding;
import com.jigsaw.solver.ProbabilityTableMaker;

public class NonAnchoredCell implements ImageCell {

    /* labels x neighbors */
    public double[][] message;

    public NonAnchoredCell(final int totalNonAnchoredNode) {
        /*
         * messages in 4 directions + from exclusion factor. One can this as the
         * message coming from neighbor factor node
         */
        message = new double[totalNonAnchoredNode][ImageGraph.NEIGHBOR + LoopyBeliefPropagationV1.TotalExclusionFactors];
        initializeMessage(totalNonAnchoredNode);
    }

    private void initializeMessage(final int totalNonAnchoredNode) {
        for (int i = 0; i < totalNonAnchoredNode; i++) {
            for (int j = 0; j < ImageGraph.NEIGHBOR + LoopyBeliefPropagationV1.TotalExclusionFactors; j++) {
                message[i][j] = 0.5;
            }
        }
    }

    @Override
    public int getBestLabel() {
        int totalNonAnchored = message.length;
        double belief = 0, currentbelief = 1;
        int bestLabel = 0;
        for (int i = 0; i < totalNonAnchored; i++) {// the labels
            currentbelief = 1;
            for (int j = 0; j < ImageGraph.NEIGHBOR + LoopyBeliefPropagationV1.TotalExclusionFactors; j++) {
                currentbelief = currentbelief * message[i][j];
                // currentbelief =
                // Thresholding.beliefThresholding(currentbelief);
            }
            // System.out.println();
            if (belief < currentbelief) {
                belief = currentbelief;
                bestLabel = i;
            }
        }
        // System.out.print("Belief:" + belief + " Patch:");
        return bestLabel;
    }

    @Override
    public boolean isAnchored() {
        return false;
    }

    @Override
    /* changed */
    public double sendMessageToNeighbour(Integer patch, final ArrayList<Integer> freeLabels,
            final ProbabilityTableMaker potentialTable, final Direction direction) {

        if ("factor".equals(direction.getName())) {
            int nonAnchorLabel = freeLabels.get(patch);

            // send message to the factor node
            double messageToFactor = 1;

            for (int i = 0; i < ImageGraph.NEIGHBOR + LoopyBeliefPropagationV1.TotalExclusionFactors; i++) {
                messageToFactor = messageToFactor * message[nonAnchorLabel][i];
                // thresholding
                messageToFactor = Thresholding.beliefThresholding(messageToFactor);
            }
            return messageToFactor;
        }
        // otherwise send the message to the specific neighbor i.e messages from
        // all directions with the edge potential
        double messageToNode = 0;
        /* which non-anchor patch it is? */
        for (int i = 0; i < freeLabels.size(); i++) {
            if (freeLabels.get(i) == -1) {
                continue;
            }
            double localMessage = potentialTable.getPotential(patch, i, direction);
            for (int j = 0; j < ImageGraph.NEIGHBOR + LoopyBeliefPropagationV1.TotalExclusionFactors; j++) {
                if (j != direction.getValue()) {
                    localMessage = localMessage * message[freeLabels.get(i)][j];
                    localMessage = Thresholding.messageThresholding(localMessage);
                }
            }
            /* MAP Inference Algorithm */
            messageToNode = Math.max(messageToNode, localMessage);
        }
        return messageToNode;
    }

    @Override
    public void normalizeMessage() {
        double normalizeFactor = 0;
        normalizeFactor = getNormalizeFactor();
//        if(normalizeFactor<1){
//            return;
//        }
        double alpha = Math.pow(normalizeFactor, 0.25);

        for (int i = 0; i < message.length; i++) {
            for (int j = 0; j < ImageGraph.NEIGHBOR; j++) {
                message[i][j] = message[i][j] / alpha;
                // thresholding
                message[i][j] = Thresholding.messageThresholding(message[i][j]);
            }
        }
    }

    @Override
    public double getNormalizeFactor() {
        double normalizeFactor = 0;
        for (int i = 0; i < message.length; i++) {
            double tempNormalizefactor = 1;
            for (int j = 0; j < ImageGraph.NEIGHBOR; j++) {
                tempNormalizefactor = tempNormalizefactor * message[i][j];
                // thresholding
                // tempNormalizefactor =
                // Thresholding.beliefThresholding(tempNormalizefactor);
            }
            normalizeFactor = normalizeFactor + tempNormalizefactor;
        }
        return normalizeFactor;
    }

    @Override
    public double setMessage(int patch, final ArrayList<Integer> freeLabels, Direction direction, double updatedMessage) {
        // thresholding
        updatedMessage = Thresholding.messageThresholding(updatedMessage);

        int nonAnchorLabel = freeLabels.get(patch);
        double epsilon = Math.abs(message[nonAnchorLabel][direction.getValue()] - updatedMessage);
        message[nonAnchorLabel][direction.getValue()] = updatedMessage;
        return epsilon;
    }
}
