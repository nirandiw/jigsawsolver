package com.jigsaw.modelInference.probabilisticGraph;

public enum Direction {
    LEFT("left", 0), RIGHT("right", 1), TOP("up", 2), BOTTOM("down", 3), FACTOR("factor", 4);

    private int value;
    private String name;

    Direction(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName(){
        return name;
    }
    public int getValue() {
        return value;
    }
}
