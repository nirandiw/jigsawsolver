package com.jigsaw.modelInference.probabilisticGraph;

import java.util.ArrayList;

import com.jigsaw.solver.ProbabilityTableMaker;

public interface ImageCell {

    // returns the best label to be assigned
    public int getBestLabel();

    // returns TRUE if this is an anchor patch
    public boolean isAnchored();

    public double sendMessageToNeighbour(Integer patch, ArrayList<Integer> freeLabels,
            ProbabilityTableMaker potentialTable, Direction direction);

    public void normalizeMessage();

    public double setMessage(int patch, final ArrayList<Integer> freeLabels,Direction direction, double message);

    public double getNormalizeFactor();

}
