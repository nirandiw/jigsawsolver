package com.jigsaw.modelInference.probabilisticGraph;

import java.util.ArrayList;

import com.jigsaw.modelInference.Thresholding;
import com.jigsaw.solver.ProbabilityTableMaker;

public class AnchoredCell implements ImageCell {

    public final int patchNumber;

    /**
     * messages in 4 directions + from exclusion factor. One can this as the
     * message coming from neighbor factor node
     */
    public double[] message;

    public AnchoredCell(Integer patchNumber) {
        this.patchNumber = patchNumber;
        message = new double[ImageGraph.NEIGHBOR];
        initializeMessage();
    }

    /**
     * initialize with any number in [0,1]. Here we are initializing with 0.5
     */
    private void initializeMessage() {
        for (int i = 0; i < message.length; i++) {
            message[i] = 0.5;
        }
    }

    @Override
    public int getBestLabel() {
        return patchNumber;
    }

    @Override
    public boolean isAnchored() {
        return true;
    }

    @Override
    public double sendMessageToNeighbour(Integer myPatch, ArrayList<Integer> freeLabels,
            ProbabilityTableMaker potentialTable, Direction direction) {
        double messageToNeighbor = potentialTable.getPotential(myPatch, patchNumber, direction);
        for (int i = 0; i < message.length; i++) {
            if (i != direction.getValue()) {
                messageToNeighbor = messageToNeighbor * message[i];
                messageToNeighbor = Thresholding.messageThresholding(messageToNeighbor);
            }
        }
        return messageToNeighbor;
    }

    @Override
    public void normalizeMessage() {
        return;
    }

    @Override
    public double setMessage(int patch, final ArrayList<Integer> freeLabels, Direction direction, double updatedMessage) {
        if (patch != patchNumber) {
            System.out.println("ERROR: patch != patchNumber");
        }
        updatedMessage = Thresholding.messageThresholding(updatedMessage);
        double epsilon = Math.abs(message[direction.getValue()] - updatedMessage);
        message[direction.getValue()] = updatedMessage;
        return epsilon;
    }

    @Override
    public double getNormalizeFactor() {
        return 0;
    }

}
