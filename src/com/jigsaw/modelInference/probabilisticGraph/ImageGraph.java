package com.jigsaw.modelInference.probabilisticGraph;

import java.util.ArrayList;
import java.util.HashMap;

import com.jigsaw.imageUtils.ImageUtils;

public class ImageGraph {

    public static int NEIGHBOR = 4;

    /* the height and width of the image grid */
    final public int gridHeight, gridWidth;
    /* image is represented as a grid with each patch as the grid node */
    public ImageCell[][] grid;

    public int totalNonAnchoredPatch;
    /* an array of size 'totalNonAnchoredPatch': a mapping */
    public ArrayList<Integer> nonAnchoredPatches;
    public ArrayList<Integer> freeLabels;

    /**
     * 
     * @param imageUtils
     * @param anchorNodes
     *            a hash map to map the specific graph node and the
     *            corresponding value(patch). Its a <location, patch value> pair
     */
    public ImageGraph(final ImageUtils imageUtils, final HashMap<Integer, Integer> anchorNodes) {
        this.gridHeight = imageUtils.getMaxPatchHeight();
        this.gridWidth = imageUtils.getMaxPatchWidth();

        initializeNonAnchoredPatches(imageUtils, anchorNodes);
        initializeGraph(anchorNodes);

        // test
        System.out.println("non anchored pathes" + totalNonAnchoredPatch);
    }

    private void initializeNonAnchoredPatches(ImageUtils imageUtils, HashMap<Integer, Integer> anchorNodes) {
        this.totalNonAnchoredPatch = imageUtils.getTotalPatchNumber() - anchorNodes.size();
        this.nonAnchoredPatches = new ArrayList<Integer>();
        this.freeLabels = new ArrayList<Integer>();
        int count =0;
        for (int i = 0; i < imageUtils.getTotalPatchNumber(); i++) {
            // if the patch is not listed as an anchor patch, insert it into a
            // non-anchor patch
           // System.out.print("patch number : "+ i);
            if (!anchorNodes.containsValue(i)) {
                freeLabels.add(count);
             //   System.out.println(" free label : " + count + " non-anchor label : " + i);
                count++;
                nonAnchoredPatches.add(i);

            } else {
                freeLabels.add(-1);
               // System.out.println(" free label :  -1  non-anchor label : " + "no entry");
            }
        }
        
        //testing
//        for(int i=0;i<nonAnchoredPatches.size();i++){
//            int node = nonAnchoredPatches.get(i);
//            System.out.println("here" + freeLabels.get(node) + "," +i);
//        }
        
        
        System.out.println("counter"+count);
    }

    private void initializeGraph(final HashMap<Integer, Integer> anchorNodes) {

        grid = new ImageCell[gridHeight][gridWidth];

        for (int i = 0; i < gridHeight; i++) {
            for (int j = 0; j < gridWidth; j++) {
                // If the node is Anchored
                if (anchorNodes.get(i * gridWidth + j) != null) {
                    System.out.println("anchor again " + (i * gridWidth + j));
                    grid[i][j] = new AnchoredCell(anchorNodes.get(i * gridWidth + j));
                } else {
                    grid[i][j] = new NonAnchoredCell(totalNonAnchoredPatch);
                }
            }
        }
    }
}
