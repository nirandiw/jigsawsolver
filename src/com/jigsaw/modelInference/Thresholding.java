package com.jigsaw.modelInference;

public class Thresholding {
    
    public static double messageThresholding(double message){
        if(message > 1E5){
            return 1E5;
        }
        if(message < 1E-5){
            return 1E-5;
        }
        return message;
    }

    public static double beliefThresholding(double belief){
        if(belief > 1E6){
            return 1E6;
        }
        if(belief < 1E-50){
            return 1E-50;
        }
        return belief;        
    }
    
    public static double factorThresholding(double factor){
        if(factor > 1){
            return 1;
        }
        
        if(factor < 1E-50){
            return 1E-50;
        }
        return factor;        
    }
}