package com.jigsaw.modelInference;

import com.jigsaw.generalUtils.Pair;
import com.jigsaw.imageUtils.ImageUtils;
import com.jigsaw.modelInference.probabilisticGraph.Direction;
import com.jigsaw.modelInference.probabilisticGraph.ImageGraph;
import com.jigsaw.solver.ProbabilityTableMaker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

/**
 * @author jay
 * 
 */
public class LoopyBeliefPropagationV1 {

    public static final int TotalExclusionFactors = 1;

    ImageGraph graph;

    /* maximum number of iterations allowed before reporting the result */
    int maxIneration = 60;

    /* minimum number of iterations before checking the exit condition */
    int minIteration = 1;

    /* desired precision */
    double epsilon;
    double[] exclusionFactor;

    ProbabilityTableMaker potentialTable;
    double[][] likelihood = null;

    public LoopyBeliefPropagationV1(ImageUtils imageUtils, HashMap<Integer, Integer> anchorNodes,
            ProbabilityTableMaker potentialTable) {
        // Initialize the graph
        graph = new ImageGraph(imageUtils, anchorNodes);
        this.potentialTable = potentialTable;
        initializeExlusion();
    }

    public void setLikelihood(double[][] likelihood) {
        this.likelihood = likelihood;
    }

    private void initializeExlusion() {
        exclusionFactor = new double[graph.totalNonAnchoredPatch];
        for (int i = 0; i < exclusionFactor.length; i++) {
            exclusionFactor[i] = 1;
        }
    }

    /* ++trick */
    public int[][] assignLabel() {
        int[][] assignedLabel = new int[graph.gridHeight][graph.gridWidth];
        // assign the best assignment first
        for (int i = 0; i < graph.gridHeight; i++) {
            System.out.println();
            for (int j = 0; j < graph.gridWidth; j++) {
                // calculate best assignment
                if (!graph.grid[i][j].isAnchored()) {
                    assignedLabel[i][j] = graph.nonAnchoredPatches.get(graph.grid[i][j].getBestLabel());
                    System.out.print(" " + assignedLabel[i][j]);
                } else {
                    assignedLabel[i][j] = graph.grid[i][j].getBestLabel();
                    System.out.print(" -1");
                }
            }
        }
        return assignedLabel;
    }

    boolean initialize = true;

    public void run() {
        int iterationCount = 0;
        /* initialized with a large value */
        double currentEpsilon = 100;

        ArrayList<Pair> nodeList = new ArrayList<Pair>();
        ArrayList<Pair> randomList = new ArrayList<Pair>();

        while (exitChecker(iterationCount++, currentEpsilon)) {
            currentEpsilon = 0;
            System.out.println(iterationCount + ": ");
            // random initialize node sequences
            randomSequenceGenerator(nodeList, randomList);
            /* for each variable node */

            for (int i = 0; i < randomList.size(); i++) {
                double tempEpsilon = passMessage(randomList.get(i).height, randomList.get(i).width);
                if (tempEpsilon > currentEpsilon)
                    currentEpsilon = tempEpsilon;
            }
            System.out.println();
            initialize = false;
            assignLabel();
        }
    }

    /**
     * Get messages for the node (@param height, @param width) from its
     * neighbor.
     */
    private double passMessage(final int height, final int width) {
        double currentEpsilon = 0;

        if (graph.grid[height][width].isAnchored()) {
            currentEpsilon = nodeMessageUpdation(height, width, graph.grid[height][width].getBestLabel());
            return currentEpsilon;
        }

        if (!graph.grid[height][width].isAnchored()) {
            // get message from the factor node
            double normalize = graph.grid[height][width].getNormalizeFactor();
            int bestAssignment = graph.grid[height][width].getBestLabel();

            for (int i = 0; i < exclusionFactor.length; i++) {
                int actualLabel = graph.nonAnchoredPatches.get(i);

                if (i == bestAssignment && initialize == false) {
                    double belief = graph.grid[height][width].sendMessageToNeighbour(graph.nonAnchoredPatches.get(i),
                            graph.freeLabels, potentialTable, Direction.FACTOR);
                    if (likelihood != null) {
                        belief = belief * likelihood[height * graph.gridWidth + width][actualLabel];
                    }
                    belief = belief / normalize;
                    exclusionFactor[i] = exclusionFactor[i] / (1 - belief);
                    exclusionFactor[i] = Thresholding.factorThresholding(exclusionFactor[i]);
                }

                double factorToNode = exclusionFactor[i];

                if (likelihood != null) {
                    factorToNode = factorToNode * likelihood[height * graph.gridWidth + width][actualLabel];
                }

                graph.grid[height][width].setMessage(graph.nonAnchoredPatches.get(i), graph.freeLabels,
                        Direction.FACTOR, factorToNode);
            }

            // get messages from all four neighbor node
            for (int i = 0; i < exclusionFactor.length; i++) {
                double tempEps = nodeMessageUpdation(height, width, graph.nonAnchoredPatches.get(i));
                if (tempEps > currentEpsilon) {
                    currentEpsilon = tempEps;
                }
            }

            // normalize for the next iteration
            graph.grid[height][width].normalizeMessage();

            // update the exclusive belief
            bestAssignment = graph.grid[height][width].getBestLabel();
            double belief = graph.grid[height][width].sendMessageToNeighbour(
                    graph.nonAnchoredPatches.get(bestAssignment), graph.freeLabels, potentialTable, Direction.FACTOR);

            int actualLabel = graph.nonAnchoredPatches.get(bestAssignment);

            if (likelihood != null) {
                belief = belief * likelihood[height * graph.gridWidth + width][actualLabel];
            }
            normalize = graph.grid[height][width].getNormalizeFactor();
            belief = belief / normalize;

            exclusionFactor[bestAssignment] = exclusionFactor[bestAssignment] * (1 - belief);
            exclusionFactor[bestAssignment] = Thresholding.factorThresholding(exclusionFactor[bestAssignment]);
        }
        return currentEpsilon;
    }

    /**
     * Parameter @param(height) and @param width specifies the specific node of
     * the grid. Here we are calculating Message(@param patchLabel) for the
     * specific node
     * 
     * @return max change of messages for this node
     */
    private double nodeMessageUpdation(final int height, final int width, final int patchLabel) {
        //System.out.print("h-w : "+ height +" , " + width +" ");
        double message;
        double currentEps = 0, tempEps;
        if (validNode(height, width - 1)) {
            message = graph.grid[height][width - 1].sendMessageToNeighbour(patchLabel, graph.freeLabels,
                    potentialTable, Direction.RIGHT);
            tempEps = graph.grid[height][width].setMessage(patchLabel, graph.freeLabels, Direction.LEFT, message);
            if (tempEps > currentEps) {
                currentEps = tempEps;
            }
        }

        if (validNode(height, width + 1)) {
            message = graph.grid[height][width + 1].sendMessageToNeighbour(patchLabel, graph.freeLabels,
                    potentialTable, Direction.LEFT);
            tempEps = graph.grid[height][width].setMessage(patchLabel, graph.freeLabels, Direction.RIGHT, message);
            if (tempEps > currentEps) {
                currentEps = tempEps;
            }
        }

        if (validNode(height + 1, width)) {
            message = graph.grid[height + 1][width].sendMessageToNeighbour(patchLabel, graph.freeLabels,
                    potentialTable, Direction.TOP);
            tempEps = graph.grid[height][width].setMessage(patchLabel, graph.freeLabels, Direction.BOTTOM, message);
            if (tempEps > currentEps) {
                currentEps = tempEps;
            }
        }

        if (validNode(height - 1, width)) {
            message = graph.grid[height - 1][width].sendMessageToNeighbour(patchLabel, graph.freeLabels,
                    potentialTable, Direction.BOTTOM);
            tempEps = graph.grid[height][width].setMessage(patchLabel, graph.freeLabels, Direction.TOP, message);
            if (tempEps > currentEps) {
                currentEps = tempEps;
            }
        }
        return currentEps;
    }

    private boolean validNode(int height, int width) {
        if ((height < graph.gridHeight && height >= 0) && (width < graph.gridWidth && width >= 0))
            return true;
        return false;
    }

    private void randomSequenceGenerator(ArrayList<Pair> nodeList, ArrayList<Pair> randomList) {
        nodeList.clear();
        randomList.clear();

        for (int i = 0; i < graph.gridHeight; i++) {
            for (int j = 0; j < graph.gridWidth; j++) {
                nodeList.add(new Pair(i, j));
            }
        }
        
        
        while (nodeList.size() > 0) {
            int random = getRandom(nodeList.size());
            randomList.add(nodeList.get(random));
            nodeList.remove(random);
        }
    }

    private int getRandom(int range) {
        Random rand = new Random();
        return rand.nextInt(range);
    }

    double exclusionSum = 0;

    private boolean exitChecker(final int iterationCount, final double currentEpsilon) {
        double tempSum = 0;
        for (int i = 0; i < exclusionFactor.length; i++) {
            tempSum += exclusionFactor[i];
        }
        System.out.println(tempSum - exclusionSum);
        exclusionSum = tempSum;

        if (iterationCount < minIteration) {
            // System.out.println("ret false");
            return true;
        } else if (iterationCount < maxIneration) {
            if (currentEpsilon == epsilon) {
                return true;
            } else {
                return true;
            }
        }
        return false;
    }
}
