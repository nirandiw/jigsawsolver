package com.jigsaw.lowResolutionImage;

import com.jigsaw.imageUtils.ImageUtils;

public class LowResolutionImageCreator {

    /* size of low resolution image */
    int lowResolutionHeight;
    int lowResolutionWidth;

    /**
     * All the low resolution images should have same height and width. Specify
     * the desired value.
     * 
     * @param myLowResolutionHeight
     * @param myLowResolutionWidth
     */
    public LowResolutionImageCreator(final int myLowResolutionHeight, final int myLowResolutionWidth) {
        lowResolutionHeight = myLowResolutionHeight;
        lowResolutionWidth = myLowResolutionWidth;
    }

    /**
     * For each image, update/create the instance of @param imageUtils and pass
     * it as output.
     * 
     * @return the low-resolution image
     */
    public int[][] create(ImageUtils imageUtils) {
        double rowWeight = ((double) lowResolutionHeight) / ((double) imageUtils.getImageHeight());
        double columnWeight = ((double) lowResolutionWidth) / ((double) imageUtils.getImageWidth());

        double[][] lowResolutionDouble = new double[lowResolutionHeight][lowResolutionWidth];

        for (int h = 0; h < imageUtils.getImageHeight(); h++) {
            for (int w = 0; w < imageUtils.getImageWidth(); w++) {
                updateForPixel(imageUtils, rowWeight, columnWeight, lowResolutionDouble, h, w);
            }
        }

        int[][] lowResolution = new int[lowResolutionHeight][lowResolutionWidth];
        for (int i = 0; i < lowResolutionHeight; i++) {
            for (int j = 0; j < lowResolutionWidth; j++) {
                lowResolution[i][j] = doubleToInt(lowResolutionDouble[i][j]);
            }
        }
        return lowResolution;
    }

    /**
     * Simply convert to the nearest integer value. Update that integer with
     * desired alpha.
     * 
     * @param pixelDouble
     * @return
     */
    private int doubleToInt(double pixelDouble) {
        int pixel;
        if ((pixelDouble - Math.floor(pixelDouble)) > 0.5) {
            pixel = (int) Math.ceil(pixelDouble);
        } else {
            pixel = (int) Math.floor(pixelDouble);
        }
        // TODO check the alpha value and update!!
        pixel = (pixel & 0x00FFFFFF) | 0x0F000000;
        return pixel;
    }

    /**
     * For each pixels at coordinate(@param h, @param w), update the particular
     * pixel(s) of the low-resolution image.
     */
    private void updateForPixel(ImageUtils imageUtils, double rowWeight, double columnWeight,
            double[][] lowResolutionImage, int h, int w) {
        double[][] rowSplit = split(h, rowWeight);
        double[][] pixelSpliter = split(w, columnWeight);

        for (int i = 0; i < 2; i++) {
            if (rowSplit[i][1] != 0) {

                for (int j = 0; j < 2; j++) {
                    if (pixelSpliter[j][1] != 0) {
                        lowResolutionImage[(int) rowSplit[i][0]][(int) pixelSpliter[j][0]] += imageUtils.getPixel(h, w)
                                * rowSplit[i][1] * pixelSpliter[j][1] * rowWeight * columnWeight;
                    }
                }
            }
        }
    }

    /**
     * for each pixel of the original image, split it's weight with appropriate
     * value.
     * 
     * @param position
     * @param weight
     * @return
     */
    private double[][] split(int position, double weight) {
        double[][] splitPixel = new double[2][2];

        splitPixel[0][0] = Math.floor(position * weight);
        splitPixel[1][0] = Math.floor((position + 1) * weight);

        if ((splitPixel[1][0] != splitPixel[0][0])
                && (Math.floor((position + 1) * weight) != Math.ceil((position + 1) * weight)))
        {
            splitPixel[0][1] = (Math.ceil(position * weight) - position * weight) / weight;
            splitPixel[1][1] = 1 - splitPixel[0][1];

        } else {
            splitPixel[0][1] = 1;
            splitPixel[1][1] = 0;
        }

        /* hack to remove numerical error */
        if (splitPixel[0][1] > 0.95) {
            splitPixel[0][1] = 1;
            splitPixel[1][1] = 0;
        }
        return splitPixel;
    }
}
