package com.jigsaw.lowResolutionImage;

import java.io.File;

import com.jigsaw.imageUtils.*;

public class CalculateProb {

	public static double[] get_avg(int[][] patch) {

		int avg_red = 0, avg_green = 0, avg_blue = 0;
		double[] avg = new double[3];
		for (int i = 0; i < patch.length; i++) {
			for (int j = 0; j < patch.length; j++) {
				int[] k = IntegerToRGBConverter.getComponents(patch[i][j]);
				avg_red += k[0];
				avg_green += k[1];
				avg_blue += k[2];
			}
		}
		avg[0] = avg_red / (patch.length * patch.length);
		avg[1] = avg_green / (patch.length * patch.length);
		avg[2] = avg_blue / (patch.length * patch.length);

		//System.out.println("get patch average rgb: "+ avg[0] + " " + avg[1] + " " + avg[2]);

		return avg;
	}

	public static double[][] calculate_Likelihood1(int[][] low, ImageUtils original) {

		int p = 0, q = 0;
		double val = 0;
		double sigma = 20;
		double[][] prob = new double[low.length * low[0].length][low.length
				* low[0].length];

		for (int i = 0; i < low.length; i++) {// height
			for (int j = 0; j < low[0].length; j++) {// width

				int[] k = IntegerToRGBConverter.getComponents(low[i][j]);

				for (int x = 0; x < original.getMaxPatchHeight(); x++) {
					for (int y = 0; y < original.getMaxPatchWidth(); y++) {

						double[] l = get_avg(original.getImagePatch(x, y));
						System.out.println("K values : " + k[0] + " " + k[1] + " " + k[2]);
						System.out.println("L values : " + l[0] + " " + l[1] + " " + l[2]);
						val = Math.abs((k[0] - l[0]))
								+ Math.abs((k[1] - l[1]))
								+ Math.abs((k[2] - l[2]));
						val = -val / (2 * sigma);

						prob[p][q] = Math.exp(val);
						q++;
					}
				}
				p++;
				q = 0;
			}
		}

		return prob;
	}
	public static double[][] calculate_Likelihood2(int[][] low, ImageUtils original) {

		int p = 0, q = 0;
		double val = 0;
		double sigma = 100;
		double[][] prob = new double[low.length * low[0].length][low.length
				* low[0].length];

		for (int i = 0; i < low.length; i++) {// height
			for (int j = 0; j < low[0].length; j++) {// width

				int[] k = IntegerToRGBConverter.getComponents(low[i][j]);

				for (int x = 0; x < original.getMaxPatchHeight(); x++) {
					for (int y = 0; y < original.getMaxPatchWidth(); y++) {

						double[] l = get_avg(original.getImagePatch(x, y));
						System.out.println("K values : " + k[0] + " " + k[1] + " " + k[2]);
						System.out.println("L values : " + l[0] + " " + l[1] + " " + l[2]);
						val = Math.abs((k[0] - l[0]))
								+ Math.abs((k[1] - l[1]))
								+ Math.abs((k[2] - l[2]));
						val = -val / (2 * sigma * sigma);

						prob[p][q] = Math.exp(val);
						q++;
					}
				}
				p++;
				q = 0;
			}
		}

		return prob;
	}
	
//	public static double[][] calculateLikelihood(int[][] low,
//			ImageUtils original) {
//
//		int p = 0, q = 0;
//		double val = 0;
//		double sigma = 0.4;
//		double[][] prob = new double[low.length * low[0].length][low.length
//				* low[0].length];
//
//		int[][] low_original = original.getImagePatch(0, 0);
//
//		for (int i = 0; i < low_original.length; i++) {// height
//			for (int j = 0; j < low_original[0].length; j++) {// width
//
//				int[] k = IntegerToRGBConverter.getComponents(low[i][j]);
//
//				for (int x = 0; x < low_original.length; x++) {
//					for (int y = 0; y < low_original[0].length; y++) {
//
//						int[] l = IntegerToRGBConverter
//								.getComponents(low_original[x][y]);
//						val = Math.pow((k[0] - l[0]), 2)
//								+ Math.pow((k[1] - l[1]), 2)
//								+ Math.pow((k[2] - l[2]), 2);
//						val = -val / (2 * sigma * sigma);
//
//						prob[p][q] = Math.exp(val);
//						q++;
//					}
//				}
//				p++;
//				q = 0;
//			}
//		}
//
//		return prob;
//	}

	public static int[][] lowResolutionImageArray(final File lowResolutionImage) {

		ImageUtils imageUtils = new ImageUtils();
		imageUtils
				.createImageRGB(imageUtils.setImage(lowResolutionImage, 1, 1));
		return imageUtils.getImageBlock();
	}
}
