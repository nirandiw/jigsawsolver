package com.jigsaw.lowResolutionImage;

import com.jigsaw.generalUtils.ConstantValues;
import com.jigsaw.generalUtils.FileUtils;
import com.jigsaw.imageUtils.ImagePatch;
import com.jigsaw.imageUtils.ImageUtils;
import com.jigsaw.imageUtils.IntegerToRGBConverter;
import com.jigsaw.imageUtils.RescaleImage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nirandikawanigasekara on 31/10/14.
 */
public class LowResolutionTrainer {

    private static int height=ConstantValues.PATCH_HEIGHT;
    private static int width=ConstantValues.PATCH_WIDTH;
    private static int lowResolutionHeight=ConstantValues.LOWRES_HEIGHT;
    private static int lowResolutionWidth=ConstantValues.LOWRES_WIDTH;
    private static int MAX_COUNT=10;
    private static File sourceDirectory = new File("ImageFolder");
    private static List<String> imageList;

    public static void main(String args[]){
        try {

        	createSameSizeImages();
        	
            imageList=FileUtils.setPath(sourceDirectory);
            ImageUtils imageUtils=new ImageUtils();
            ImagePatch imagePatch=new ImagePatch(imageUtils,imageList, height,width);

            /*Do the clustering*/
            Cluster cluster= new Cluster(height,width,imagePatch);
           /* cluster.initClusterCenters();
            cluster.doClustering();
            cluster.writeClusterCentersToFile();
            cluster.setClusterCounterToZero();*/
            doIterativeClustering(cluster);

            /*creating the histogram*/
            imagePatch.reset();
            Histogram histogram = new Histogram(imagePatch, height,width, cluster);
            histogram.generateHistograms();

            /*Creating RGB files for low resolution image as requested by animesh*/
            int currentImageNumber=0;
            File currentImage;
            boolean isFolderFull=true;

            File outputFileRED= new File("lowresolutionimageRED.txt");
            if(!outputFileRED.exists()){
                outputFileRED.createNewFile();
            }
            FileWriter fileWriterRED=new FileWriter(outputFileRED.getAbsoluteFile(), true);
            BufferedWriter bufferedReaderRED= new BufferedWriter(fileWriterRED);


            File outputFileGREEN= new File("lowresolutionimageGREEN.txt");
            if(!outputFileGREEN.exists()){
                outputFileGREEN.createNewFile();
            }
            FileWriter fileWriterGREEN=new FileWriter(outputFileGREEN.getAbsoluteFile(), true);
            BufferedWriter bufferedReaderGREEN= new BufferedWriter(fileWriterGREEN);

            File outputFileBLUE= new File("lowresolutionimageBLUE.txt");
            if(!outputFileBLUE.exists()){
                outputFileBLUE.createNewFile();
            }
            FileWriter fileWriterBLUE=new FileWriter(outputFileBLUE.getAbsoluteFile(), true);
            BufferedWriter bufferedReaderBLUE= new BufferedWriter(fileWriterBLUE);


            while(isFolderFull){
                if (imageList.size() >currentImageNumber) {
                    currentImage = new File(imageList.get(currentImageNumber));
                    BufferedImage originalImage = ImageIO.read(currentImage);
                    int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
                    BufferedImage resizeImagePng = RescaleImage.resizeImage(originalImage, type, lowResolutionWidth, lowResolutionHeight);
                    File outputFile=new File(ConstantValues.LOW_RESOLUTION_IMAGE_OUTPUT_PATH+currentImageNumber+".png");
                    ImageIO.write(resizeImagePng, "png",outputFile);
                    imageUtils.createImageRGB(imageUtils.setImage(outputFile, lowResolutionHeight, lowResolutionWidth));

                    for(int i=0;i<lowResolutionHeight;i++){
                        for(int j=0;j<lowResolutionWidth;j++){
                            int[] rgbComponents=IntegerToRGBConverter.getComponents(imageUtils.getPixel(i, j));
                           // System.out.print(rgbComponents[0]+" ");
                           // System.out.print(rgbComponents[1]+" ");
                           // System.out.print(rgbComponents[2]+" ");

                            bufferedReaderRED.write(rgbComponents[0]+" ");
                            bufferedReaderGREEN.write(rgbComponents[1]+" ");
                            bufferedReaderBLUE.write(rgbComponents[2]+" ");

                        }
                    }

                    currentImageNumber++;
                }else{
                    isFolderFull=false;
                }
                bufferedReaderRED.write("\n");
                bufferedReaderGREEN.write("\n");
                bufferedReaderBLUE.write("\n");
            }

            bufferedReaderRED.close();
            bufferedReaderGREEN.close();
            bufferedReaderBLUE.close();
            fileWriterRED.close();
            fileWriterGREEN.close();
            fileWriterBLUE.close();

            // cluster.printClusterCenters(clusterCentersList);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static ArrayList<ClusterCenter> doIterativeClustering(Cluster cluster) {
        ArrayList<ClusterCenter> clusterCentersList=new ArrayList<ClusterCenter>();

        int numberOfClusteringIterations=0;
        cluster.initClusterCenters();
        while(numberOfClusteringIterations<MAX_COUNT){
               clusterCentersList= cluster.doClustering();
               cluster.setClusterCounterToZero();
               numberOfClusteringIterations++;
               System.out.println("doIterativeClustering"+numberOfClusteringIterations);
        }
        try {
            cluster.writeClusterCentersToFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return clusterCentersList;
    }
  
    private static void createSameSizeImages() {
		// TODO Auto-generated method stub
        File sourceDirectory = new File(ConstantValues.ORIGINAL_IMAGE_PATH);
        List<String>tmpImageList=null;
    	try {
			tmpImageList=FileUtils.setPath(sourceDirectory);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        boolean isFolderFull=true;
        int currentImageNumber=0;
        
        while(isFolderFull){
        	if (tmpImageList.size() >currentImageNumber) {
                File tmpCurrentImage = new File(tmpImageList.get(currentImageNumber));
                BufferedImage originalImage;
				try {
					originalImage = ImageIO.read(tmpCurrentImage);
	                int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
	                BufferedImage resizeImagePng = RescaleImage.resizeImage(originalImage, type, 1000, 500);
	                File outputFile=new File("ImageFolder/"+currentImageNumber+".png");
	                ImageIO.write(resizeImagePng, "png",outputFile);
	                currentImageNumber++;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

            } else {
            	isFolderFull=false;
            }
        }
       
	}

}
