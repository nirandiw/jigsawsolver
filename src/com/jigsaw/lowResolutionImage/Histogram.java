package com.jigsaw.lowResolutionImage;

import com.jigsaw.generalUtils.ConstantValues;
import com.jigsaw.generalUtils.FileUtils;
import com.jigsaw.imageUtils.ImagePatch;
import com.jigsaw.imageUtils.ImageUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nirandikawanigasekara on 4/11/14.
 */
public class Histogram {
    private static int height=ConstantValues.PATCH_HEIGHT;
    private static int width=ConstantValues.PATCH_WIDTH;
    private ImagePatch myImagePatch;
    private static int patchHeight;
    private static int patchWidth;
    private Cluster cluster;


    public Histogram(ImagePatch imagePatch, int myPatchHeight, int myPatchWidth, Cluster myCluster) {
        patchHeight=myPatchHeight;
        patchWidth=myPatchWidth;
        cluster=myCluster;
        myImagePatch=imagePatch;


    }
    
    public Histogram(ArrayList<ClusterCenter> listOfCC) {
        cluster=new Cluster(ConstantValues.PATCH_HEIGHT,ConstantValues.PATCH_WIDTH , listOfCC);
        cluster.setClusterCounterToZero();
        /*for(ClusterCenter cc: cluster.getListOfClusterCenters()){
        	System.out.println(cc.clusterCounter+"Nirandi");
        }*/
    }



    public void generateHistograms(){
        boolean isNotEnd=true;
        int currentImageNumber=0;
        while(isNotEnd){
            int[][] imagePatch=myImagePatch.getNextImagePatch();
            if(imagePatch==null){
                isNotEnd=false;
            }else {
                if(currentImageNumber==myImagePatch.getImageCounter()){
                    cluster.doClusteringPerPatch(imagePatch, true);
                } else{
                    try {
                        writeHistogramToFile(cluster.getListOfClusterCenters(), currentImageNumber);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    cluster.setClusterCounterToZero();
                    cluster.doClusteringPerPatch(imagePatch, true);
                    currentImageNumber++;
                }
            }


        }
    }

    public void writeHistogramToFile(ArrayList<ClusterCenter> listOfClusterCenters, int imageCounter) throws IOException {

        File outputFile= new File("histogram.txt");
        if(!outputFile.exists()){
            outputFile.createNewFile();
        }

        FileWriter fileWriter=new FileWriter(outputFile.getAbsoluteFile(), true);
        BufferedWriter bufferedReader= new BufferedWriter(fileWriter);
        //bufferedReader.write(" "+imageCounter);
        for (ClusterCenter clusterCenter:listOfClusterCenters){
            bufferedReader.write(clusterCenter.clusterCounter+" ");
        }
        bufferedReader.write("\n");
        bufferedReader.close();
        fileWriter.close();

    }

    public int[] generateHistogramsForTheProblem(ImageUtils myImageUtils){
    	
    	for (int i=0;i<myImageUtils.getMaxPatchHeight();i++) {
    		for (int j=0;j<myImageUtils.getMaxPatchWidth();j++) {
    			int[][] imagePatch = myImageUtils.getImagePatch(i, j);
                cluster.doClusteringPerPatch(imagePatch, true);
                
    		}
    	}
    	int[] problemHistogram=new int[cluster.numberOfClusterCenters];
    	int counter=0;
    	for (ClusterCenter clusterCenter:cluster.getListOfClusterCenters()){
    		
            problemHistogram[counter]= clusterCenter.clusterCounter;
            counter++;
        }
    	return problemHistogram;
    }

    /*int[][] imagePatch=getNextPatch();

    cluster.do*/


}
