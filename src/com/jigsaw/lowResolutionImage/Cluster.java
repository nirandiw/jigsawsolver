package com.jigsaw.lowResolutionImage;


import java.io.*;
import java.util.ArrayList;

import com.jigsaw.imageUtils.ImagePatch;
import com.jigsaw.imageUtils.IntegerToRGBConverter;

/**
 * Created by nirandikawanigasekara on 29/10/14.
 */
public class Cluster {
    /*initialize the k cluster centers z1, ..., zk in any way
    create counters  n1, ..., nk and initialize them to zero
    loop
    get new data point x
    determine the closest center zi to x
    update the number of points in that cluster:
    update the cluster center: zi �? zi + 1 (x − zi) ni
    end loop*/


    int patchHeight=0;
    int patchWidth=0;
    int numberOfClusterCenters=5;
    private ArrayList<ClusterCenter> listOfClusterCenters=new ArrayList<ClusterCenter>();
    ImagePatch myImagePatch;

    public Cluster(int height, int width, ImagePatch imagePatch){
        this.patchHeight=height;
        this.patchWidth=width;
        myImagePatch=imagePatch;
    }


    /*created as requested by Harshit to return the cluster a patch belongs too. In the constructor I can also read
    from a file. I did not implement it yet*/

    public Cluster(int height, int width,ArrayList<ClusterCenter> listOfCC){
        this.patchHeight=height;
        this.patchWidth=width;
        listOfClusterCenters=listOfCC;
    }



    public ArrayList<ClusterCenter> doClustering(){
      //  System.out.println("doClustering");
        //initClusterCenters();
       boolean isNotEnd=true;
       int patchCounter=0;
        while(isNotEnd){
            int[][] imagePatch=myImagePatch.getNextImagePatch();

            if(imagePatch==null){
                isNotEnd=false;
            }else {
                patchCounter++;
                imagePatch=getRGBValueForPatch(imagePatch);
                doClusteringPerPatch(imagePatch,false);
            }
        }
        System.out.println("Number of patches"+patchCounter);
        return listOfClusterCenters;
    }

    private int[][] getRGBValueForPatch(int[][] imagePatch) {
        int[] patchComponents;
        for(int i=0;i<patchHeight;i++){
            for(int j=0;j<patchWidth;j++){
                patchComponents=IntegerToRGBConverter.getComponents(imagePatch[i][j]);
                int red=patchComponents[0];
                int green=patchComponents[1];
                int blue=patchComponents[2];
                imagePatch[i][j]=IntegerToRGBConverter.getIntFromColor(red, green, blue);
                //System.out.print(imagePatch[i][j]);
            }
        }
        return imagePatch;
    }

    public int doClusteringPerPatch(int[][] imagePatch, boolean isFindingClosestCluster) {
    //    System.out.println("doClusteringPerPatch");
        double minEuclideanDist=Double.MAX_VALUE;

        int closestClusterCenterIndex=0;
        for (ClusterCenter clusterCenter :listOfClusterCenters){
            double euclideanDist= getEuclideanDistance(imagePatch, clusterCenter);
            if(euclideanDist< minEuclideanDist){
                closestClusterCenterIndex=listOfClusterCenters.indexOf(clusterCenter);
                minEuclideanDist=euclideanDist;
            }
        }
        listOfClusterCenters.get(closestClusterCenterIndex).clusterCounter++;
        //System.out.println(listOfClusterCenters.get(closestClusterCenterIndex).clusterCounter);//TODO check with Harshit if this counter is not increased and impact rest of it.
        if(isFindingClosestCluster==false){
            calculateNewClusterCenter(listOfClusterCenters.get(closestClusterCenterIndex), imagePatch);
        }

        return closestClusterCenterIndex; //Added as requested by Harishit.
    }

    private void calculateNewClusterCenter(ClusterCenter clusterCenter, int[][] imagePatch) {
       // System.out.println("calculateNewClusterCenter");
        for(int i=0;i<patchHeight;i++){
            for (int j=0;j<patchWidth;j++){
                clusterCenter.patchRGB[i][j]
                        =clusterCenter.patchRGB[i][j]
                        +((imagePatch[i][j]-clusterCenter.patchRGB[i][j])/clusterCenter.clusterCounter);
            }
        }
    }

    private double getEuclideanDistance(int[][] imagePatch, ClusterCenter clusterCenter ) {
        //System.out.println("getEuclideanDistance");
        int rgbDiff=0;
        int rgbDiffSquare=0;
        double euclideanDist = 0;
        for (int i=0;i<patchHeight;i++){
            for (int j=0;j<patchWidth;j++){
                rgbDiff=imagePatch[i][j]-clusterCenter.patchRGB[i][j];
                rgbDiffSquare+= rgbDiff*rgbDiff;
                euclideanDist=Math.sqrt(rgbDiffSquare);
            }
        }
        return euclideanDist;
    }

    public void initClusterCenters() {
        System.out.println("initClusterCenters");
        for(int k=0;k<numberOfClusterCenters;k++){
            ClusterCenter clusterCenterPatch=new ClusterCenter(patchHeight,patchWidth);
            for(int i=0;i<patchHeight;i++){
                for(int j=0;j<patchWidth;j++){
                    clusterCenterPatch.patchRGB[i][j]=IntegerToRGBConverter.getRandomColor();
                }
            }
            clusterCenterPatch.isClusterCenter=true;
            listOfClusterCenters.add(clusterCenterPatch);
        }
        System.out.println("Size of listOfClusterCenter"+listOfClusterCenters.size());

    }

    public void writeClusterCentersToFile() throws IOException {

        File outputFile= new File("clustercenters.txt");
        if(!outputFile.exists()){
            outputFile.createNewFile();
        }

        FileWriter fileWriter=new FileWriter(outputFile.getAbsoluteFile());
        BufferedWriter bufferedReader= new BufferedWriter(fileWriter);
      for (ClusterCenter clusterCenter:listOfClusterCenters){
          bufferedReader.write(" "+listOfClusterCenters.indexOf(clusterCenter));
          for(int i=0;i<patchHeight;i++){
              for(int j=0;j<patchWidth;j++){
                    bufferedReader.write(" "+clusterCenter.patchRGB[i][j]);
              }
              bufferedReader.write("\n");
          }
          bufferedReader.write("\n\n");
      }
        bufferedReader.close();
        fileWriter.close();

    }

    public void printClusterCenters(ArrayList<ClusterCenter> clusterCentersList) {
        for(ClusterCenter clusterCenter:clusterCentersList){
            for(int i=0;i<patchHeight;i++){
                for(int j=0;j<patchWidth;j++){
                    System.out.print(clusterCenter.patchRGB[i][j]);
                }
                System.out.println(" ");
            }
            System.out.println(" ");
            System.out.println(" ");
        }
    }


   public void setClusterCounterToZero(){
       for(ClusterCenter clusterCenter:listOfClusterCenters){
           clusterCenter.clusterCounter=0;
       }
   }

    public ArrayList<ClusterCenter> getListOfClusterCenters(){
        return  listOfClusterCenters;
    }
}
