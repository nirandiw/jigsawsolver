package com.jigsaw.Regression;

import java.util.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;

import com.jigsaw.generalUtils.ConstantValues;

public class LinearRegression {
	// Gradient descent
	// ----------------

	MyVector x;
	MyVector y;
	double learningRate;
	double[][] delta_theta;
	double[][] theta;
	int no_of_samples;
	int no_of_pixels_in_low_resolution = ConstantValues.LOWRES_HEIGHT * ConstantValues.LOWRES_WIDTH;
	int no_of_cluster_centers = 5;
	String component = null;
	public static int done = 0;

	// file X
	// file Y

	// Constructor

	public LinearRegression(String component) {
		// this.x = new Vector(x);
		// this.y = new Vector(y);
		// this.theta = theta;
		this.initialize_theta();
		this.component = component;
	}

	public static int gen_random() {
		int minimum = 1;
		int maximum = 10;

		return minimum + (int) (Math.random() * maximum);
	}

	private void initialize_theta() {
		// Decide a random Start Point
		this.theta = new double[no_of_pixels_in_low_resolution][no_of_cluster_centers + 1]; // Theta1
																							// -
																							// Thetan
																							// and
																							// Theta0
		for (int i = 0; i < no_of_pixels_in_low_resolution; i++)
			// No of pixels in Low resolution Image
			for (int j = 0; j < no_of_cluster_centers + 1; j++) // Theta1 -
																// Thetan and
																// Theta0
			{
				this.theta[i][j] = gen_random();
			}

		this.delta_theta = new double[this.theta.length][this.theta[0].length];
	}

	public void setSamplesCount(int count) {
		this.no_of_samples = count;
	}

	public void setLearningRate(double alpha) {
		this.learningRate = alpha;
	}

	public double getLearningRate() {
		return learningRate;
	}

	private double getX(int i) {
		return this.x.get(i);
	}

	private double getY(int i) {
		return this.y.get(i);
	}

	public void setXY(double[] x, double[] y) {
		this.x = new MyVector(x);
		this.y = new MyVector(y);
	}

	public void setTheta(double[][] theta) {
		this.theta = theta;
	}

	public double[][] getTheta() {
		return theta;
	}

	private double getH(int i) {
		double h = 0;
		h += theta[i][0];

		for (int j = 0; j < x.size(); j++) {
			h += getX(j) * theta[i][j + 1];
		}

		return h;
	}

	public double[][] gradientDescent() {

		for (int i = 0; i < this.y.size(); i++) {
			double diff = getH(i) - getY(i);
			this.delta_theta[i][0] = diff * 1;
			for (int j = 0; j < this.x.size(); j++) {
				this.delta_theta[i][j + 1] += diff * getX(j);
			}

		}
		return delta_theta;

	}

	public void updateTheta() {
		// Updating Parameters Theta

		// System.out.println("Updated theta");
		for (int k = 0; k < this.y.size(); k++)
			for (int j = 0; j < this.x.size() + 1; j++) {
				this.theta[k][j] = this.theta[k][j]
						- (this.learningRate / this.no_of_samples)
						* this.delta_theta[k][j];

				// System.out.println(this.theta[k][j]);
			}

	}

	public void printTheta() {
		// Updating Parameters Theta
		for (int k = 0; k < this.y.size(); k++)
			for (int j = 0; j < this.x.size() + 1; j++) {
				System.out.println(this.theta[k][j] + " / "
						+ this.delta_theta[k][j] + " ");

			}
		System.out.println("______________________________________");

	}

	public static double[] predict_component(int[] histogram1,
			double[][] theta, int size_of_low_resolution_image) {
		
		double[] histogram = new double[histogram1.length]; 
		
		for (int j = 0; j < histogram1.length; j++) {
		histogram[j] = histogram1[j]; 
		}
		
		
		double[] low_resolution_image = new double[size_of_low_resolution_image];
		for (int i = 0; i < size_of_low_resolution_image; i++) {
			double h = 0;
			h += theta[i][0];

			for (int j = 0; j < histogram.length; j++) {
				//System.out.println(h + "+=   "+ histogram[j] + "* " + theta[i][j + 1] );
				h += histogram[j] * theta[i][j + 1];
				
				
			}
			

			low_resolution_image[i] = h;

		}
		return low_resolution_image;

	}

	public double computeCost() {
		double diff = 0;
		for (int i = 0; i < this.y.size(); i++) {
			diff += getH(i) - getY(i);

		}
		return diff;

	}

	public double[][] run() {

		BufferedReader br_input, br_target;
		String line_input, line_target;

		try {
			br_input = new BufferedReader(new FileReader("histogram.txt"));
			br_target = new BufferedReader(new FileReader(this.component));

			for (int a = 0; a < this.no_of_samples; a++) {
				// x = Read Histogram
				// y = Read Low Resolution
				double[] x = null;
				double[] y = null;

				if ((line_input = br_input.readLine()) != null) {
					String input[] = line_input.split(" ");
					x = new double[input.length];
					for (int i = 0; i < input.length; i++) {
						x[i] = Double.parseDouble(input[i]);

					}

				}

				//int counter = 0;
				if ((line_target = br_target.readLine()) != null) {
					String target[] = line_target.split(" ");
					y = new double[target.length];
					for (int i = 0; i < target.length; i++) {
						y[i] = Double.parseDouble(target[i]);
						 //counter++;
						 //System.out.println(y[i]+ "    " + counter);
					}

				}

				setXY(x, y);
				gradientDescent();
			}

			br_input.close();
			br_target.close();

		} catch (IOException e) {
			System.out.println("Cant read File.");
			return this.theta;
		}

		// System.out.print("Before");
		// this.printTheta();

		this.updateTheta();

		// System.out.print("After");
		// this.printTheta();

		int cost = 0;

		try {
			br_input = new BufferedReader(new FileReader("histogram.txt"));
			br_target = new BufferedReader(new FileReader(this.component));

			for (int a = 0; a < this.no_of_samples; a++) {
				// x = Read Histogram
				// y = Read Low Resolution
				double[] x = null;
				double[] y = null;

				if ((line_input = br_input.readLine()) != null) {
					String input[] = line_input.split(" ");
					x = new double[input.length];
					for (int i = 0; i < input.length; i++) {
						x[i] = Double.parseDouble(input[i]);

					}

				}

				if ((line_target = br_target.readLine()) != null) {
					String target[] = line_target.split(" ");
					y = new double[target.length];
					for (int i = 0; i < target.length; i++) {
						// System.out.println("kdh ");
						y[i] = Double.parseDouble(target[i]);
						// System.out.println(y[i]+ " ");
					}

				}

				setXY(x, y);
				cost += computeCost();
			}

			if (cost==0)
				done =1;
			
			System.out.print("Error in this Iteration:");
			System.out.println(cost);

			br_input.close();
			br_target.close();

		} catch (IOException e) {
			System.out.println("Cant read File.");
			return this.theta;
		}

		return this.theta;
	}
}
