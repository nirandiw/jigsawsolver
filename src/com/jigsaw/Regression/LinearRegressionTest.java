package com.jigsaw.Regression;

import java.lang.*;
import java.util.Scanner;
import java.io.BufferedWriter;
import java.io.Writer;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.File;
import java.io.IOException;

import com.jigsaw.generalUtils.ConstantValues;

public class LinearRegressionTest {
	static int iterations = 50000; // Tuneable
	static int no_of_samples = 15; // Tuneable
	static LinearRegression univariate;
	static double learningRate = 0.00005;

	public static void main(String[] args)

	{

		int no_of_pixels_in_low_resolution = ConstantValues.LOWRES_HEIGHT * ConstantValues.LOWRES_WIDTH;
		int no_of_cluster_centers = 5;
		double[][] theta = new double[no_of_pixels_in_low_resolution][no_of_cluster_centers + 1];

		String[] files = { "lowresolutionimageRED.txt",
				"lowresolutionimageGREEN.txt", "lowresolutionimageBLUE.txt" };

		for (String element : files) {
			// Loop till number of iterations
			for (int i = 0; i < iterations && LinearRegression.done!=1; i++) {

				// Loop on all samples
				// X --> Histogram
				// Y ---> Low Resolution Image
				// m ---> count of samples

				univariate = new LinearRegression(element);
				univariate.setLearningRate(learningRate);
				if (i != 0)
					univariate.setTheta(theta);
				univariate.setSamplesCount(no_of_samples);
				theta = univariate.run();

				// Update Learning Rate learning Rate based on the current
				// learned stuffs

			}
			LinearRegression.done = 0;

			Writer writer = null;

			try {
				writer = new BufferedWriter(new OutputStreamWriter(
						new FileOutputStream(element.substring(0,
								element.length() - 4)
								+ "theta.txt"), "utf-8"));

				for (int i = 0; i < no_of_pixels_in_low_resolution; i++)
					// No of pixels in Low resolution Image
					for (int j = 0; j < no_of_cluster_centers + 1; j++) // Theta1
																		// -
																		// Thetan
																		// and
																		// Theta0
					{
						writer.write(String.valueOf(theta[i][j]) + " ");
					}

			} catch (IOException ex) {
				// report
			} finally {
				try {
					writer.close();
				} catch (Exception ex) {
				}
			}

		}
		// Testing

		/*
		 * double [] result1 = predict(x1, theta, 12); double [] result2 =
		 * predict(x2, theta, 12);
		 * 
		 * for (int i=0; i<y2.length; i++ ) { System.out.println(result1[i]);
		 * System.out.println(y1[i]); System.out.println("------");
		 * System.out.println(result2[i]); System.out.println(y2[i]);
		 * System.out.println("+++++++++");
		 * 
		 * 
		 * }
		 */

	}

}
