package com.jigsaw.analysis;

import com.jigsaw.imageUtils.ImageUtils;
import com.jigsaw.imageUtils.IntegerToRGBConverter;
import com.jigsaw.lowResolutionImage.Cluster;
import com.jigsaw.lowResolutionImage.ClusterCenter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class ErrorAnalysis {

    public static int valid_patch(final ImageUtils imageUtils, int i, int j) {

        if (i < 0 || j < 0 || i >= imageUtils.getMaxPatchHeight() || j >= imageUtils.getMaxPatchWidth())
            return 0;
        else
            return 1;
    }

    public static int diff(int a, int b) {
        if (a > b)
            return (a - b);
        else
            return (b - a);
    }

    public static int compare_patch(int[][] patch1, int[][] patch2) {



        for (int i = 0; i < patch1.length; i++) {
            for (int j = 0; j < patch1[0].length; j++) {
                int[] component1= IntegerToRGBConverter.getComponents(patch1[i][j]);
                int[] component2 = IntegerToRGBConverter.getComponents(patch2[i][j]);
                
                for(int k=0;k<3;k++){
                    if(component1[k]!=component2[k]){
                        return 0;
                    }
                }
            }
        }
        return 1;
    }

    public static int compare_patch_cluster(int[][] patch1, int[][] patch2) {

        ArrayList<ClusterCenter> listOfClusterCenters = new ArrayList<ClusterCenter>();

        ClusterCenter center_patch = new ClusterCenter(patch1.length, patch1[0].length);
        try {
            Scanner in = new Scanner(new File("D:/intellij_projects/Final_Project/clustercenters.txt"));
            for (int i = 0; i < 5; i++) {
                in.nextInt();
                for (int k = 0; k < patch1.length; k++) {
                    for (int l = 0; l < patch1[0].length; l++) {
                        center_patch.patchRGB[k][l] = in.nextInt();
                    }
                }
                for (int k = 0; k < patch1.length; k++) {
                    for (int l = 0; l < patch1[0].length; l++) {
                        System.out.print(center_patch.patchRGB[k][l] + " ");
                    }
                    System.out.println();
                }
                System.out.println();
                listOfClusterCenters.add(center_patch);
            }
            in.close();
        } catch (IOException e) {
        }

        Cluster tmpCluster = new Cluster(patch1.length, patch1[0].length, listOfClusterCenters);
        int index1 = tmpCluster.doClusteringPerPatch(patch1, true);
        int index2 = tmpCluster.doClusteringPerPatch(patch2, true);

        if (index1 == index2)
            return 1;
        else
            return 0;

    }

    public static double direct_compare(final ImageUtils imageUtils1, final ImageUtils imageUtils2) {

        int count_correct = 0;
        int count_wrong = 0;
        double accuracy;

        for (int i = 0; i < imageUtils1.getMaxPatchHeight(); i++) {
            for (int j = 0; j < imageUtils1.getMaxPatchWidth(); j++) {

                if (compare_patch(imageUtils1.getImagePatch(i, j), imageUtils2.getImagePatch(i, j)) == 1)
                    count_correct++;
                else
                    count_wrong++;
            }
        }

        accuracy = (count_correct*100) / (double)(count_correct + count_wrong);
        return accuracy;
    }

    public static double cluster_compare(final ImageUtils imageUtils1, final ImageUtils imageUtils2) {

        int count_correct = 0;
        int count_wrong = 0;
        double error;

        for (int i = 0; i < imageUtils1.getMaxPatchHeight(); i++) {
            for (int j = 0; j < imageUtils1.getMaxPatchWidth(); j++) {

                if (compare_patch_cluster(imageUtils1.getImagePatch(i, j), imageUtils2.getImagePatch(i, j)) == 1)
                    count_correct++;
                else
                    count_wrong++;
            }
        }
        error = (count_wrong * 100) / (count_correct + count_wrong);
        return error;
    }

    public static double neighbors_comparison(final ImageUtils imageUtils1, final ImageUtils imageUtils2) {

        double accuracy, sum = 0;

        for (int i = 0; i < imageUtils1.getMaxPatchHeight(); i++) {
            for (int j = 0; j < imageUtils1.getMaxPatchWidth(); j++) {

                int count = 0, total = 0;
                
                if (valid_patch(imageUtils1, i, j + 1) == 1 && valid_patch(imageUtils2, i, j + 1) == 1) {
                    if (compare_patch(imageUtils1.getImagePatch(i, j + 1), imageUtils2.getImagePatch(i, j + 1)) == 1)
                        count++;
                    total++;
                }
                if (valid_patch(imageUtils1, i + 1, j) == 1 && valid_patch(imageUtils2, i + 1, j) == 1) {
                    if (compare_patch(imageUtils1.getImagePatch(i + 1, j), imageUtils2.getImagePatch(i + 1, j)) == 1)
                        count++;
                    total++;
                }
                if (valid_patch(imageUtils1, i, j - 1) == 1 && valid_patch(imageUtils2, i, j - 1) == 1) {
                    if (compare_patch(imageUtils1.getImagePatch(i, j - 1), imageUtils2.getImagePatch(i, j - 1)) == 1)
                        count++;
                    total++;
                }
                if (valid_patch(imageUtils1, i - 1, j) == 1 && valid_patch(imageUtils2, i - 1, j) == 1) {
                    if (compare_patch(imageUtils1.getImagePatch(i - 1, j), imageUtils2.getImagePatch(i - 1, j)) == 1)
                        count++;
                    total++;
                }
                sum += count / total;
            }
        }

        accuracy = (sum * 100) / (imageUtils1.getTotalPatchNumber());
        return accuracy;

    }

    public static double neighbor_compare(final ImageUtils imageUtils1, final ImageUtils imageUtils2) {

        int left = 0, right = 0, top = 0, bottom = 0;
        double accuracy, fraction, sum = 0;
        int[][] checked = new int[imageUtils1.getMaxPatchHeight()][imageUtils1.getMaxPatchWidth()];

        for (int k = 0; k < imageUtils1.getMaxPatchHeight(); k++) {
            for (int l = 0; l < imageUtils1.getMaxPatchWidth(); l++) {
                checked[k][l] = 0;
            }
        }

        int flag = 0;

        for (int k = 0; k < imageUtils1.getMaxPatchHeight(); k++) {
            for (int l = 0; l < imageUtils1.getMaxPatchWidth(); l++) {

                flag = 0;

                for (int i = 0; i < imageUtils2.getMaxPatchHeight(); i++) {
                    for (int j = 0; j < imageUtils2.getMaxPatchWidth(); j++) {

                        if (compare_patch(imageUtils1.getImagePatch(k, l), imageUtils2.getImagePatch(i, j)) == 1
                                && checked[i][j] == 0)
                        {

                            int count = 0;

                            if (valid_patch(imageUtils1, k, l + 1) == 1 && valid_patch(imageUtils2, i, j + 1) == 1) {
                                right = compare_patch(imageUtils1.getImagePatch(k, l + 1),
                                        imageUtils2.getImagePatch(i, j + 1));
                                count++;
                            }
                            if (valid_patch(imageUtils1, k + 1, l) == 1 && valid_patch(imageUtils2, i + 1, j) == 1) {
                                bottom = compare_patch(imageUtils1.getImagePatch(k + 1, l),
                                        imageUtils2.getImagePatch(i + 1, j));
                                count++;
                            }
                            if (valid_patch(imageUtils1, k, l - 1) == 1 && valid_patch(imageUtils2, i, j - 1) == 1) {
                                left = compare_patch(imageUtils1.getImagePatch(k, l - 1),
                                        imageUtils2.getImagePatch(i, j - 1));
                                count++;
                            }
                            if (valid_patch(imageUtils1, k - 1, l) == 1 && valid_patch(imageUtils2, i - 1, j) == 1) {
                                top = compare_patch(imageUtils1.getImagePatch(k - 1, l),
                                        imageUtils2.getImagePatch(i - 1, j));
                                count++;
                            }
                            if (count != 0) {
                                fraction = (left + right + bottom + top) / count;
                                sum += fraction;
                            }
                            checked[i][j] = 1;
                            flag = 1;
                            break;
                        }
                    }
                    if (flag == 1)
                        break;
                }
            }
        }
        accuracy = (sum * 100) / imageUtils1.getTotalPatchNumber();
        return accuracy;
    }

}
